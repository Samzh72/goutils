package goutils

import (
	"os"
	"runtime/pprof"
)

// ProfilingStart starts profiling and will generate log file
// log file name is always profile.log.
// to analysis the log, run:
//     go tool pprof your_exe profile.log
func ProfilingStart() {
	f, _ := os.Create("profile.log")
	pprof.StartCPUProfile(f)
}

// ProfilingStop stops profiling
// as well it prints the log how you could analysis the profiling log
func ProfilingStop() {
	pprof.StopCPUProfile()
	LogInfo("go tool pprof your_exe profile.log")
}
