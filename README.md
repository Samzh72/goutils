For Beginners
=============

###Source Orgnization

GOPATH shoudl be set to a unique place for all Go projects:

export GOPATH=$HOME/go

/home/sam/go
    bin/    1. go helpers required by IDE(vim or VS) go here. 2. 'go install' will install binaries here
    pkg/    intermediate binary files
    src/    source files of not a single project, but all Go projects
        github.com/ 3rd party libraries
        golang.org/ non-official golang support packages
        ...
        bitbucket.org/  my personal Go projects
            Samzh72/    all my Go projects are hosted by bitbucket.org, 
                        each of them could be a repo or project in bitbucket.org
                /goutils    one of my utility Go library
                /xxx        my project which may refer to goutils/


###Package Reference

import project internal packages:

```
import "bitbucket.org/Samzh72/xxx/config"
```
you have to specify the full path (from $GOPATH/src) to the package source, even it is a project internal package.

import other packages:

```
import "bitbucket.org/Samzh72/goutils"
```
whatever the package is, import it by full path (from $GOPATH/src)
