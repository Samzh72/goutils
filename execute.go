package goutils

import (
	"bufio"
	"os"
	"runtime/debug"
	"time"
)

// CallStack prints the call stack to standard error
// it doesn't like other log functions with timestamp and module name
// it just print directly without decoration
func CallStack() {
	debug.PrintStack()
}

// Sleep will sleep for a while
// the unit of duration is second, because this function is used for
// debugging only
func Sleep(second int32) {
	time.Sleep(time.Duration(second) * time.Second)
}

// AnyKey will block execution until user pressed RETURN key
// only RETURN key will unblock the execution for now because to simulate
// getch() is too heavy in Go
func AnyKey(prompt string) {
	LogInfo("press any key: %s", prompt)
	reader := bufio.NewReader(os.Stdin)
	reader.ReadString('\n')
}
